# ansible-test

I've got a couple Raspberry Pi's so lets see what we can do with ansible.
Ansible is installed on my host system and the IP address of the raspberry pi's are listd in the hosts file.
A secure shell key has been added to each pi to allow access.

Test the conection with `ansible rpi -m ping`

```
chris@chewie:~/projects/ansible-test$ ansible rpi -m ping                                                                       
192.168.12.101 | SUCCESS => {                                                                                                       "ansible_facts": {                                                                                                          
        "discovered_interpreter_python": "/usr/bin/python"                                                                      
    },                                                                                                                          
    "changed": false,                                                                                                           
    "ping": "pong"                                                                                                              
}                                                                                                                               
192.168.12.103 | SUCCESS => {                                                                                                   
    "ansible_facts": {                                                                                                          
        "discovered_interpreter_python": "/usr/bin/python3"                                                                     
    },                                                                                                                              "changed": false,                                                                                                               "ping": "pong"                                                                                                              
}
```

## Troubleshooting

I tried running the update OS playbook and both devices had errors:

```
chris@chewie:~/projects/ansible-test$ ansible-playbook update-os.yml                                                            
                                                                                                                                
PLAY [Upgrade the OS (apt-get dist-upgrade)] ***********************************************************************************
                                                                                                                                
TASK [Gathering Facts] *********************************************************************************************************
ok: [192.168.12.101]
ok: [192.168.12.103]

TASK [Update OS] ***************************************************************************************************************
fatal: [192.168.12.101]: FAILED! => {"changed": false, "msg": "ansible-core requires a minimum of Python2 version 2.7 or Python3 version 3.6. Current version: 3.5.3 (default, Nov 18 2020, 21:09:16) [GCC 6.3.0 20170516]"}
fatal: [192.168.12.103]: FAILED! => {"changed": false, "msg": "'/usr/bin/apt-get dist-upgrade ' failed: E: Could not open lock file /var/lib/dpkg/lock-frontend - open (13: Permission denied)\nE: Unable to acquire the dpkg frontend lock (/var/lib/dpkg/lock-frontend), are you root?\n", "rc": 100, "stdout": "", "stdout_lines": []}

PLAY RECAP *********************************************************************************************************************
192.168.12.101             : ok=1    changed=0    unreachable=0    failed=1    skipped=0    rescued=0    ignored=0   
192.168.12.103             : ok=1    changed=0    unreachable=0    failed=1    skipped=0    rescued=0    ignored=0   
```

### Python is too old

This pi was born long ago and is running a version of python that is too old.
I connected to the pi and tried to update with `sudo apt update && sudo apt install python3` but it is already at the latest available version.
You can also check with `apt-cache policy python3` after the `update`
